This app packages Mautic <upstream>2.13.1</upstream>.

Mautic is an open marketing software platform that provides you with the greatest level of integration and deep audience intelligence, enabling you to make more meaningful customer connections.