FROM cloudron/base:0.11.0

ENV VERSION 2.13.1

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
	apt-get install -y php7.0-bcmath && \
	rm -rf /var/cache/apt /var/lib/apt/lists

# install mautic
RUN wget https://github.com/mautic/mautic/archive/${VERSION}.tar.gz -O - | tar -xz -C /app/code --strip-components=1 && \
	chown -R www-data:www-data /app/code && \
    sudo -u www-data composer install --working-dir=/app/code --no-dev --no-interaction --no-progress && \
    sudo -u www-data composer dump-autoload --working-dir=/app/code --optimize --no-interaction && \
	ln -sf /app/data/config/local.php /app/code/app/config/local.php && \
	rm -rf /app/code/app/cache && ln -sf /run/mautic/cache /app/code/app/cache && \
	rm -rf /app/code/app/logs && ln -sf /run/mautic/logs /app/code/app/logs && \
	mv /app/code/media /app/code/media-vanilla && ln -sf /app/data/media /app/code/media && \
    mv /app/code/themes /app/code/themes-vanilla && ln -sf /app/data/themes /app/code/themes && \
    mv /app/code/plugins /app/code/plugins-vanilla && ln -sf /app/data/plugins /app/code/plugins && \
	mv /app/code/translations /app/code/translations-vanilla && ln -sf /app/data/translations /app/code/translations

ADD local.php.template /app/code/app/config/local.php.template
EXPOSE 8000


# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/mautic.conf /etc/apache2/sites-enabled/mautic.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php. apache2ctl -M can be used to list enabled modules
RUN a2enmod rewrite && \
    a2enmod expires && \
    a2enmod headers && \
    a2enmod cache
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/mautic/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/7.0/apache2/php.ini /etc/php/7.0/cli/php.ini

# Hack to stop sendmail/postfix to keep retrying sending emails because of rofs
RUN rm -rf /var/spool

ADD start.sh /app/code/
RUN chmod +x /app/code/start.sh

CMD [ "/app/code/start.sh" ]