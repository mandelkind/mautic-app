Mautic does not integrate with the Cloudron user management. After installation, there is a pre-setup admin account. The initial credentials are:

`username: admin`

`password: mautic`


**Please change this credentials after first login**