#!/bin/bash
set -eu

readonly CONSOLE="php /app/code/app/console"

echo "==> Startup"

# ensure directories
mkdir -p /run/mautic/sessions \
	/run/mautic/cache \
	/run/mautic/logs \
	/app/data/config


# Settings which should be updated only once
if [[ ! -f "/app/data/config/local.php" ]]; then
    echo "==> Creating initial /app/data/config/local.php file"
    sed -e "s|.*\(secret_key\).*|'\1' => '$(pwgen -1cns 32 | sha256sum | cut -d " " -f 1)',|g" \
        -e "s|.*\(mailer_from_name\).*|'\1' => 'Mautic',|g" \
        /app/code/app/config/local.php.template > /app/data/config/local.php # sed -i seems to destroy symlink
fi


# Settings to be updated on every run.
echo "==> Update local.php file for database and email configs"
sed -e "s|.*\(db_driver\).*|'\1' => 'pdo_mysql',|g" \
    -e "s|.*\(db_host\).*|'\1' => '${MYSQL_HOST}',|g" \
    -e "s|.*\(db_port\).*|'\1' => '${MYSQL_PORT}',|g" \
    -e "s|.*\(db_name\).*|'\1' => '${MYSQL_DATABASE}',|g" \
    -e "s|.*\(db_user\).*|'\1' => '${MYSQL_USERNAME}',|g" \
    -e "s|.*\(db_password\).*|'\1' => '${MYSQL_PASSWORD}',|g" \
    -e "s|.*\(mailer_transport\).*|'\1' => 'smtp',|g" \
    -e "s|.*\(mailer_port\).*|'\1' => '${MAIL_SMTP_PORT}',|g" \
    -e "s|.*\(mailer_encryption\).*|'\1' => null,|g" \
    -e "s|.*\(mailer_auth_mode\).*|'\1' => 'plain',|g" \
    -e "s|.*\(mailer_host\).*|'\1' => '${MAIL_SMTP_SERVER}',|g" \
    -e "s|.*\(mailer_username\).*|'\1' => '${MAIL_SMTP_USERNAME}',|g" \
    -e "s|.*\(mailer_from_email\).*|'\1' => '${MAIL_FROM}',|g" \
    -e "s|.*\(mailer_password\).*|'\1' => '${MAIL_SMTP_PASSWORD}',|g" \
    -e "s|.*\(site_url\).*|'\1' => '${APP_ORIGIN}',|g" \
    -i /app/data/config/local.php


if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "==> Copying files on first run"
    cp -r /app/code/media-vanilla /app/data/media
    cp -r /app/code/themes-vanilla /app/data/themes
    cp -r /app/code/plugins-vanilla /app/data/plugins
    cp -r /app/code/translations-vanilla /app/data/translations
    
    echo "==> Run database setup"
    $CONSOLE mautic:install:data --force --no-interaction --verbose
    $CONSOLE doctrine:migrations:version --add --all --no-interaction --verbose
    
    echo "==> Download ip lookup information"
    # initial fetch of ip lookup database. after setup this is done once per month via cron.
    $CONSOLE mautic:iplookup:download --no-interaction --verbose

    touch "/app/data/.dbsetup"
else
    echo "==> Run database migration"
    $CONSOLE doctrine:migrations:migrate --no-interaction --verbose
fi

# clear cached stuff
$CONSOLE cache:clear --no-interaction --verbose

# ensure permissions are set correctly
chown -R www-data:www-data /app/data /run

# start
echo "==> Start apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND